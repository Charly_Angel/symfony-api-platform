<?php

declare(strict_types=1);

namespace App\Api\Action\Facebook;

use App\Service\Facebook\FacebookService;
use App\Service\Request\RequestService;
use Facebook\Exceptions\FacebookSDKException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class Authorization
{
    private FacebookService $facebookService;

    public function __construct(FacebookService $facebookService)
    {
        $this->facebookService = $facebookService;
    }

    /**
     * @throws FacebookSDKException
     */
    public function __invoke(Request $request): JsonResponse
    {
        $accessToken = RequestService::getField($request, 'accessToken');

        return new JsonResponse(
            ['token' => $this->facebookService->authorize($accessToken)]
        );
    }
}
