<?php

namespace App\Service\Password;

use App\Entity\User;
use App\Exception\Password\PasswordException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class EncoderService
{
    private UserPasswordHasherInterface $userPasswordHasherInterface;
    private const MINIMUM_LENGTH = 6;

    /**
     * EncoderService constructor.
     */
    public function __construct(UserPasswordHasherInterface $userPasswordHasherInterface)
    {
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
    }

    public function generateHashPassword(UserInterface $user, string $password): string
    {
        if (self::MINIMUM_LENGTH > strlen($password)) {
            throw PasswordException::invalidLength();
        }

        return $this->userPasswordHasherInterface->hashPassword($user, $password);
    }

    public function _isValidPassword(User $user, string $oldPassword): bool
    {
        return $this->userPasswordHasherInterface->isPasswordValid($user, $oldPassword);
    }
}
