<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\User;

use App\Entity\User;
use App\Exception\Password\PasswordException;
use App\Service\User\ChangePasswordService;

class ChangePasswordServiceTest extends UserServiceTestBase
{
    private ChangePasswordService $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->service = new ChangePasswordService($this->userRepository, $this->encoderService);
    }

    public function testChangePassword(): void
    {
        $user = new User('name', 'email@api.com');
        $oldPassword = 'old-password';
        $newPassword = 'new-password';

        $this->userRepository
            ->expects($this->exactly(1))
            ->method('findByIdOrFail')
            ->with($user->getId())
            ->willReturn($user);

        $this->encoderService
            ->expects($this->exactly(1))
            ->method('_isValidPassword')
            ->with($user, $oldPassword)
            ->willReturn(true);

        $result = $this->service->changePassword($user->getId(), $oldPassword, $newPassword);

        $this->assertInstanceOf(User::class, $result);
    }

    public function testChangePasswordForInvalidOldPassword(): void
    {
        $user = new User('name', 'email@api.com');
        $oldPassword = 'old-password';
        $newPassword = 'new-password';

        $this->userRepository
            ->expects($this->exactly(1))
            ->method('findByIdOrFail')
            ->with($user->getId())
            ->willReturn($user);

        $this->encoderService
            ->expects($this->exactly(1))
            ->method('_isValidPassword')
            ->with($user, $oldPassword)
            ->willReturn(false);

        $this->expectException(PasswordException::class);

        $this->service->changePassword($user->getId(), $oldPassword, $newPassword);
    }
}
