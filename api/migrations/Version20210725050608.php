<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210725050608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'creates `users_groups`, `users_groups_users` tables and its relationships';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE `users_groups`(
                id CHAR(36) NOT NULL PRIMARY KEY,
                name VARCHAR(100) NOT NULL,
                owner_id CHAR(36) NOT NULL,
                created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                INDEX IDX_users_groups_name (name),
                INDEX IDX_users_groups_owner_id (owner_id),
                CONSTRAINT FK_users_groups_owner_id FOREIGN KEY (owner_id) REFERENCES `users`(id) ON UPDATE CASCADE ON DELETE CASCADE
            )DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB'
        );

        $this->addSql('CREATE TABLE `users_groups_users`(
                user_id CHAR(36) NOT NULL,
                group_id CHAR(36) NOT NULL,
                UNIQUE U_user_id_group_id (user_id, group_id),
                CONSTRAINT FK_users_groups_users_user_id FOREIGN KEY (user_id) REFERENCES `users`(id) ON UPDATE CASCADE ON DELETE CASCADE,
                CONSTRAINT FK_users_groups_users_group_id FOREIGN KEY (group_id) REFERENCES `users_groups`(id) ON UPDATE CASCADE ON DELETE CASCADE
            )DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB'
        );

    }

    public function down(Schema $schema): void
    {
       $this->addSql('DROP TABLE `users_groups_users`');
       $this->addSql('DROP TABLE `users_groups`');
    }
}
